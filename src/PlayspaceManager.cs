﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using Valve.VR;

namespace FamiliarTurn
{
    public static class PlayspaceManager
    {
        public static bool ResetAnimationActive { get; private set; }

        // Store total position/rotation offset applied by us
        // so that we can reset later on (without disrupting any other apps like Advanced Settings)
        private static Vector3 PositionByUs;
        private static Quaternion RotationByUs = Quaternion.Identity;

        private static float ResetAnimationTimer = 0f;

        private static Vector3 ResetAnimationStartPosition;
        private static Quaternion ResetAnimationStartRotation;

        private static Vector3 ResetAnimationEndPosition;
        private static Quaternion ResetAnimationEndRotation;

        private static ulong ResetHandle;
        private static InputDigitalActionData_t ResetData;

        static PlayspaceManager()
        {
            Utils.GetActionHandle("/actions/familiarturn/in/reset", out ResetHandle, out ResetData);
        }

        public static void RotateYAroundHMD(float deltaRadians)
        {
            // Get current universe origin
            Matrix4x4 universeOrigin = GetUniverseOrigin();

            // Rotate around HMD
            universeOrigin = DoHMDCenteredYRotation(universeOrigin, deltaRadians);

            // Publish origin
            SetUniverseOrigin(universeOrigin);
        }

        public static void Reset()
        {
            ResetAnimationActive = true;
            ResetAnimationTimer = 0f;

            // Store start & end positions/rotations for origin
            Matrix4x4 currentOrigin = GetUniverseOrigin();
            ResetAnimationStartPosition = currentOrigin.Translation;
            ResetAnimationStartRotation = currentOrigin.GetRotation();

            ResetAnimationEndPosition = currentOrigin.Translation - PositionByUs;
            ResetAnimationEndRotation = RotationByUs.Inverse() * currentOrigin.GetRotation();
        }

        public static void Update(float deltaTime)
        {
            // Update action state
            Utils.UpdateDigitalActionData(ResetHandle, ref ResetData);

            // Detect rising edge of reset press
            if (ResetData.bChanged && ResetData.bState)
            {
                Reset();
            }

            // Advance reset animation
            if (!ResetAnimationActive) { return; }
            ResetAnimationTimer += deltaTime;
            float progress = ResetAnimationTimer / SettingsManager.Settings.ResetAnimationTime;

            Vector3 position = Vector3.Lerp(ResetAnimationStartPosition, ResetAnimationEndPosition, progress);
            Quaternion rotation = Quaternion.Slerp(ResetAnimationStartRotation, ResetAnimationEndRotation, progress);

            Matrix4x4 newOrigin = MathExtensions.CreateMatrix4x4(position, rotation);
            SetUniverseOrigin(newOrigin);

            if (progress >= 1)
            {
                ResetAnimationActive = false;
                RotationByUs = Quaternion.Identity;
                PositionByUs = Vector3.Zero;
            }
        }

        public static Matrix4x4 GetUniverseOrigin()
        {
            HmdMatrix34_t universeOriginVRMat = new();

            OpenVR.ChaperoneSetup.GetWorkingStandingZeroPoseToRawTrackingPose(ref universeOriginVRMat);

            return universeOriginVRMat.ToMatrix4x4();
        }

        public static void SetUniverseOrigin(Matrix4x4 origin)
        {
            // First, calculate the delta between the current universe origin and the new origin
            Matrix4x4 currentOrigin = GetUniverseOrigin();
            Quaternion deltaRotation = origin.GetRotation() * currentOrigin.GetRotation().Inverse();
            Vector3 deltaPosition = origin.Translation - currentOrigin.Translation;

            // Then, add those deltas to our stored position/rotation offsets
            RotationByUs = RotationByUs * deltaRotation;
            PositionByUs += deltaPosition;

            // Finally, actually set the universe origin.
            HmdMatrix34_t universeOriginVRMat = origin.ToHmdMatrix34();

            OpenVR.ChaperoneSetup.SetWorkingStandingZeroPoseToRawTrackingPose(ref universeOriginVRMat);
            OpenVR.ChaperoneSetup.ShowWorkingSetPreview();
        }

        private static Matrix4x4 DoHMDCenteredYRotation(Matrix4x4 universeOrigin, float deltaRadians)
        {
            // To derive this formula I basically stumbled my way to the finish line,
            // with a bunch of logging & experimentation.

            // I'm not exactly sure why we have to *add* the universe origin,
            // or why we subtract the pivot first rather than adding it,
            // but I imagine it's to do with the universe origin
            // appearing in the HMD position, just inverted.

            // I'd like to analyze this in more detail at some point,
            // but for now, I'm just glad it works.

            Matrix4x4 hmdPose = Utils.GetHMDPose();

            Matrix4x4 deltaRotation = Matrix4x4.CreateRotationY(deltaRadians);
            Vector3 pivotPoint = PositionRelativeToPlayspace(hmdPose.Translation, universeOrigin);

            universeOrigin.Translation -= pivotPoint;
            universeOrigin *= deltaRotation;
            universeOrigin.Translation += pivotPoint;

            return universeOrigin;
        }

        public static Vector3 PositionRelativeToPlayspace(Vector3 rawPos, Matrix4x4? universeOrigin = null)
        {
            // See the comment in DoHMDCenteredYRotation

            var origin = universeOrigin ?? GetUniverseOrigin();

            return origin.GetRotation()
                         .Inverse()
                         .RotateVector(rawPos) + origin.Translation;
        }
    }
}
