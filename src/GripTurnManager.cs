﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Valve.VR;
using System.Numerics;

namespace FamiliarTurn
{
    public static class GripTurnManager
    {
        public static bool GripTurnActive;

        private static float PreviousHandAngle;

        private static ulong GripTurnEnableLeftHandle;
        private static InputDigitalActionData_t GripTurnEnableLeftData;

        private static ulong GripTurnEnableRightHandle;
        private static InputDigitalActionData_t GripTurnEnableRightData;

        private static ulong LeftHandPoseHandle;
        private static InputPoseActionData_t LeftHandPoseData;

        private static ulong RightHandPoseHandle;
        private static InputPoseActionData_t RightHandPoseData;

        static GripTurnManager()
        {
            Utils.GetActionHandle("/actions/familiarturn/in/gripTurnEnableLeft", out GripTurnEnableLeftHandle,
                                                                                 out GripTurnEnableLeftData);

            Utils.GetActionHandle("/actions/familiarturn/in/gripTurnEnableRight", out GripTurnEnableRightHandle,
                                                                                  out GripTurnEnableRightData);

            Utils.GetActionHandle("/actions/familiarturn/in/leftHandPose", out LeftHandPoseHandle,
                                                                           out LeftHandPoseData);

            Utils.GetActionHandle("/actions/familiarturn/in/rightHandPose", out RightHandPoseHandle,
                                                                            out RightHandPoseData);
        }

        public static void Update(float deltaTime, bool manipulationAllowed)
        {
            // First, update our actions.
            Utils.UpdateDigitalActionData(GripTurnEnableLeftHandle, ref GripTurnEnableLeftData);
            Utils.UpdateDigitalActionData(GripTurnEnableRightHandle, ref GripTurnEnableRightData);
            Utils.UpdatePoseActionData(LeftHandPoseHandle, ref LeftHandPoseData);
            Utils.UpdatePoseActionData(RightHandPoseHandle, ref RightHandPoseData);

            // Now, should grip turn be active?
            bool wantActive = GripTurnEnableLeftData.bState && GripTurnEnableRightData.bState;
            bool stateChanged = GripTurnEnableLeftData.bChanged || GripTurnEnableRightData.bChanged;

            if (wantActive && stateChanged)
            {
                // Enable grip turn
                GripTurnActive = true;
                PreviousHandAngle = CalculateHandsAngle();
            }
            else if (!wantActive && stateChanged)
            {
                // Disable
                GripTurnActive = false;
            }

            // Bail out if grip turn or manipulation is disabled
            if (!GripTurnActive || !manipulationAllowed) { return; }

            // Calculate delta hand angle & rotate
            float angle = CalculateHandsAngle();

            float deltaAngle = angle - PreviousHandAngle;
            if (deltaAngle < -MathF.PI) { deltaAngle += 2 * MathF.PI; }
            if (deltaAngle >= MathF.PI) { deltaAngle -= 2 * MathF.PI; }
            PreviousHandAngle = angle;

            deltaAngle *= SettingsManager.Settings.GripTurnMultiplier;

            PlayspaceManager.RotateYAroundHMD(deltaAngle);
        }

        private static float CalculateHandsAngle()
        {
            // Get left/right hand pose
            Matrix4x4 leftMatrix = LeftHandPoseData.pose.mDeviceToAbsoluteTracking.ToMatrix4x4();
            Matrix4x4 rightMatrix = RightHandPoseData.pose.mDeviceToAbsoluteTracking.ToMatrix4x4();

            Vector3 leftPos = leftMatrix.Translation;
            Vector3 rightPos = rightMatrix.Translation;

            // Calculate angle
            Vector3 subtracted = Vector3.Normalize(leftPos - rightPos);
            float angle = MathF.Atan2(subtracted.Z, subtracted.X);

            // Unrotate angle
            Matrix4x4 universeOrigin = PlayspaceManager.GetUniverseOrigin();
            Vector3 universeForward = universeOrigin.GetRotation().RotateVector(-Vector3.UnitZ); // -Z is forward in openvr
            float universeYaw = MathF.Atan2(universeForward.Z, universeForward.X);

            angle -= universeYaw;

            // Normalize angle range to [0, 2pi]
            angle = angle < 0 ? angle + 2 * MathF.PI : angle;

            return angle;
        }
    }
}
