﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using System.Text;
using System.Threading.Tasks;
using Valve.VR;

namespace FamiliarTurn
{
    public static class SnapTurnManager
    {
        public static bool SnapTurnActive => MathF.Abs(PendingRotation) > 0f;

        private static ulong RotateHandle;
        private static InputAnalogActionData_t RotateData;

        private static List<Vector2> RawRotateInputsBuffer = new();
        private static bool ThresholdDebounce;

        private static float PendingRotation;
        private static float PreviousRotationSign = 1;

        static SnapTurnManager()
        {
            Utils.GetActionHandle("/actions/familiarturn/in/rotate", out RotateHandle, out RotateData);
        }

        public static void Update(float deltaTime, bool manipulationAllowed)
        {
            // First, update action state.
            Utils.UpdateAnalogActionData(RotateHandle, ref RotateData);

            // Bail out & cancel if manipulation is disallowed
            if (!manipulationAllowed)
            {
                if (SnapTurnActive) { CancelSnapTurn(); }
                return;
            }

            // If we have any pending degrees to rotate, do that instead & bail out.
            if (SnapTurnActive) { UpdateRotation(deltaTime); return; }

            // Otherwise, let's determine the current rotate stick input
            // and see if we need to perform a rotation.
            Vector2 rawInput = new Vector2(RotateData.x, RotateData.y);
            AddToRawRotateInputsBuffer(rawInput);

            Vector2 smoothedInput = RawRotateInputsBuffer.Aggregate((a, b) => a + b) / (float)RawRotateInputsBuffer.Count;
            float threshold = SettingsManager.Settings.SnapTurnThreshold;
            
            // If the joystick is pulled down, initiate a 180 degree turn
            // in the direction of the previous non-180 snap turn.
            if (smoothedInput.Y < -threshold)
            {
                if (ThresholdDebounce) { return; }

                PendingRotation = 180f * PreviousRotationSign;
                ThresholdDebounce = true;
            }
            // Otherwise if the joystick is pulled to either side,
            // rotate in that direction.
            else if (MathF.Abs(smoothedInput.X) > threshold)
            {
                if (ThresholdDebounce) { return; }

                PreviousRotationSign = MathF.CopySign(1, smoothedInput.X);

                float increment = SettingsManager.Settings.SnapTurnIncrement;
                PendingRotation = MathF.CopySign(increment, smoothedInput.X);

                ThresholdDebounce = true;
            }
            // Finally if no options were matched, reset the debounce flag
            else if (ThresholdDebounce)
            {
                ThresholdDebounce = false;
            }
        }

        static void UpdateRotation(float deltaTime)
        {
            // Consume PendingRotation in (speed * dt) sized steps
            // to perform the animation.
            float speed = SettingsManager.Settings.SnapTurnSpeedDegreesSec;

            float consumed = MathF.Min(speed * deltaTime, MathF.Abs(PendingRotation));
            consumed = MathF.CopySign(consumed, PendingRotation);

            PendingRotation -= consumed;
            PlayspaceManager.RotateYAroundHMD(consumed * MathExtensions.DEG2RAD);
        }

        static void AddToRawRotateInputsBuffer(Vector2 rawInput)
        {
            // Add to buffer
            RawRotateInputsBuffer.Add(rawInput);

            // Remove earliest if we're exceeding the max buffer size
            int max = SettingsManager.Settings.SnapTurnInputAverageFrames;
            if (RawRotateInputsBuffer.Count <= max) { return; }

            RawRotateInputsBuffer.RemoveAt(0);
        }

        public static void CancelSnapTurn()
        {
            PendingRotation = 0f;
        }
    }
}
