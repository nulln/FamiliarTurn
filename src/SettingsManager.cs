﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace FamiliarTurn
{
    public class Settings
    {
        public float GripTurnMultiplier = 1.5f;
        public int SnapTurnInputAverageFrames = 15;
        public float SnapTurnThreshold = 0.8f;
        public float SnapTurnSpeedDegreesSec = 720f; // deg/s
        public float SnapTurnIncrement = 90f;
        public float ResetAnimationTime = 3f;
    }

    public static class SettingsManager
    {
        public static Settings Settings = new();
        public static string SettingsPath => Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "settings.json");

        private static JsonSerializerOptions JsonOptions = new() { IncludeFields = true };

        static SettingsManager()
        {
            TryLoadFromDisk();
        }

        public static void TryLoadFromDisk()
        {
            // Bail out & save the default settings to disk if we don't have a settings.json file.
            if (!File.Exists(SettingsPath)) { SaveToDisk(); return; }

            // Deserialize json from disk
            string fileContents = File.ReadAllText(SettingsPath);
            try
            {
                Settings = JsonSerializer.Deserialize<Settings>(fileContents, JsonOptions);
            }
            catch (Exception)
            {
                // Bail out & notify user on error
                Utils.Error("Could not load settings.json! Is the file corrupt?");
            }
        }

        public static void SaveToDisk()
        {
            string json = JsonSerializer.Serialize(Settings, JsonOptions);
            using (StreamWriter streamWriter = new StreamWriter(SettingsPath))
            {
                streamWriter.WriteLine(json);
            }
        }
    }
}
