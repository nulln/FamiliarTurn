﻿using System.Numerics;
using Valve.VR;

namespace FamiliarTurn
{
    static class MathExtensions
    {
        public static float DEG2RAD => MathF.PI / 180f;

        // Debug methods
        public static string ToFormattedString(this HmdMatrix34_t m)
        {
            // HmdMatrix34_t is column-major, so read DOWN
            //            X              Y             Z        Translation
            return "[[" + m.m0 + ", " + m.m1 + ", " + m.m2 + ", " + m.m3 + "],\n" +
                   " [" + m.m4 + ", " + m.m5 + ", " + m.m6 + ", " + m.m7 + "],\n" +
                   " [" + m.m8 + ", " + m.m9 + ", " + m.m10 + ", " + m.m11 + "]]";
        }

        public static string ToFormattedString(this Matrix4x4 m)
        {
            // Matrix4x4 is row-major, so read ACROSS
            return "[[" + m.M11 + ", " + m.M12 + ", " + m.M13 + ", " + m.M14 + "],\n" + // X
                   " [" + m.M21 + ", " + m.M22 + ", " + m.M23 + ", " + m.M24 + "],\n" + // Y
                   " [" + m.M31 + ", " + m.M32 + ", " + m.M33 + ", " + m.M34 + "],\n" + // Z
                   " [" + m.M41 + ", " + m.M42 + ", " + m.M43 + ", " + m.M44 + "]]";    // Translation
        }

        // Conversions between matrices
        // (transposes between column-major & row-major)
        public static Matrix4x4 ToMatrix4x4(this HmdMatrix34_t im)
        {
            Matrix4x4 result_matrix = new Matrix4x4(
                im.m0, im.m4, im.m8, 0.0f,
                im.m1, im.m5, im.m9, 0.0f,
                im.m2, im.m6, im.m10, 0.0f,
                im.m3, im.m7, im.m11, 1.0f
            );
            return result_matrix;
        }

        public static HmdMatrix34_t ToHmdMatrix34(this Matrix4x4 im)
        {
            HmdMatrix34_t result_matrix = new HmdMatrix34_t();

            result_matrix.m0 = im.M11;
            result_matrix.m4 = im.M12;
            result_matrix.m8 = im.M13;

            result_matrix.m1 = im.M21;
            result_matrix.m5 = im.M22;
            result_matrix.m9 = im.M23;

            result_matrix.m2 = im.M31;
            result_matrix.m6 = im.M32;
            result_matrix.m10 = im.M33;

            result_matrix.m3 = im.M41;
            result_matrix.m7 = im.M42;
            result_matrix.m11 = im.M43;

            return result_matrix;
        }

        // Conversions between misc types
        public static HmdVector3_t ToHmdVector3(this Vector3 vec)
        {
            return new HmdVector3_t { v0 = vec.X, v1 = vec.Y, v2 = vec.Z };
        }

        public static Vector3 ToVector3(this HmdVector3_t vec)
        {
            return new Vector3(vec.v0, vec.v1, vec.v2);
        }

        // Vector rotations
        public static Vector3 RotateVector(this Quaternion quat, Vector3 vector)
        {
            return Vector3.Transform(vector, quat);
        }

        // Misc operations
        public static Quaternion GetRotation(this Matrix4x4 matrix)
        {
            Quaternion q = new();
            
            q.W = MathF.Sqrt(MathF.Max(0, 1 + matrix.M11 + matrix.M22 + matrix.M33)) / 2;
            q.X = MathF.Sqrt(MathF.Max(0, 1 + matrix.M11 - matrix.M22 - matrix.M33)) / 2;
            q.Y = MathF.Sqrt(MathF.Max(0, 1 - matrix.M11 + matrix.M22 - matrix.M33)) / 2;
            q.Z = MathF.Sqrt(MathF.Max(0, 1 - matrix.M11 - matrix.M22 + matrix.M33)) / 2;
            q.X = MathF.CopySign(q.X, matrix.M32 - matrix.M23);
            q.Y = MathF.CopySign(q.Y, matrix.M13 - matrix.M31);
            q.Z = MathF.CopySign(q.Z, matrix.M21 - matrix.M12);

            return q;
        }

        public static Quaternion Inverse(this Quaternion q)
        {
            return Quaternion.Inverse(q);
        }

        // Constructors
        public static Matrix4x4 CreateMatrix4x4(Vector3 position, Quaternion rotation)
        {
            Matrix4x4 m = Matrix4x4.Identity;
            
            // X
            Vector3 x = rotation.RotateVector(-Vector3.UnitX);
            m.M11 = x.X;
            m.M12 = x.Y;
            m.M13 = x.Z;

            // Y
            Vector3 y = rotation.RotateVector(Vector3.UnitY);
            m.M21 = y.X;
            m.M22 = y.Y;
            m.M23 = y.Z;

            // Z
            Vector3 z = rotation.RotateVector(Vector3.UnitZ);
            m.M31 = z.X;
            m.M32 = z.Y;
            m.M33 = z.Z;

            // Translation
            m.M41 = position.X;
            m.M42 = position.Y;
            m.M43 = position.Z;

            return m;
        }
    }
}
