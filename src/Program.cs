﻿using System.Diagnostics;
using System.Numerics;
using System.Runtime.InteropServices;
using Valve.VR;

namespace FamiliarTurn
{
    internal class Program
    {
        static void Main(string[] args)
        {
            // Initialize OpenVR
            var initError = EVRInitError.None;
            OpenVR.Init(ref initError, EVRApplicationType.VRApplication_Overlay);
            if (initError != EVRInitError.None) { Utils.Error("Failed to launch OpenVR!"); }

            // Get input action set
            OpenVR.Input.SetActionManifestPath(Utils.GetResourceFile("actionmanifest.json"));

            ulong actionSetHandle = 0;
            var actionError = OpenVR.Input.GetActionSetHandle("/actions/familiarturn", ref actionSetHandle);
            if (actionError != EVRInputError.None) { Utils.Error("Unable to get action set!"); }

            // Use obtained handle to create a VRActiveActionSet
            var activeActionSet = new VRActiveActionSet_t
            {
                ulActionSet = actionSetHandle,
                ulRestrictedToDevice = OpenVR.k_ulInvalidInputValueHandle
            };

            // Then create an array of action sets, containing only our active action set
            var actionSets = new VRActiveActionSet_t[1] { activeActionSet };
            var actionSetsSize = (uint)Marshal.SizeOf(activeActionSet);

            // Obtain display frequency
            ETrackedPropertyError propErr = ETrackedPropertyError.TrackedProp_Success;
            float displayFrequency = OpenVR.System.GetFloatTrackedDeviceProperty(OpenVR.k_unTrackedDeviceIndex_Hmd,
                                                                                 ETrackedDeviceProperty.Prop_DisplayFrequency_Float,
                                                                                 ref propErr);
            if (propErr != ETrackedPropertyError.TrackedProp_Success) { Utils.Error("Error obtaining HMD display frequency!"); }

            // Finally, with setup done, we can begin our application logic
            // and enter the main loop
            while (true)
            {
                // HACK(?): Increasing the timeout here improves performance & remove
                // any jittering under load, presumably because frames can take longer to render.
                OpenVR.Overlay.WaitFrameSync((uint)(1000 / displayFrequency) * 5);

                // Update action state
                EVRInputError updErr = OpenVR.Input.UpdateActionState(actionSets, actionSetsSize);
                if (updErr != EVRInputError.None) { Utils.Error("Failed to update action state!"); }

                // Now we can do our application stuff
                float deltaTime = DeltaTimeManager.NewFrame();

                // First, we'll try updating grip turn state.
                bool manipulationAllowed = !PlayspaceManager.ResetAnimationActive;

                GripTurnManager.Update(deltaTime, manipulationAllowed);

                // Then, if we're now in a grip turn, we'll cancel the active snap turn (if active).
                // Otherwise, update snap turn state.
                if (SnapTurnManager.SnapTurnActive && GripTurnManager.GripTurnActive)
                {
                    SnapTurnManager.CancelSnapTurn();
                }
                else if (!GripTurnManager.GripTurnActive)
                {
                    SnapTurnManager.Update(deltaTime, manipulationAllowed);
                }

                // Finally, update PlayspaceManager to process reset binding/advance reset animation
                PlayspaceManager.Update(deltaTime);
            }
        }
    }
}