﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Numerics;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Valve.VR;

namespace FamiliarTurn
{
    public static class Utils
    {
        public static void Error(string message)
        {
            Console.WriteLine(message);

            // Throw exception if we've got a debugger
            if (Debugger.IsAttached)
            {
                Console.WriteLine("A debugger is attached, throwing exception..");
                throw new Exception(message);
            }

            // Otherwise do nothing for eternity
            Console.WriteLine("Feel free to close this window..");
            while (true)
            {
                Console.ReadKey();
            }
        }

        public static string GetResourceFile(string fileName)
        {
            return Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "resources", fileName);
        }

        public static void GetActionHandle<T>(string actionName, out ulong actionHandle, out T actionData) where T : new()
        {
            // Defaults
            actionHandle = 0;
            actionData = default;

            // Get action handle
            EVRInputError err = OpenVR.Input.GetActionHandle(actionName, ref actionHandle);
            if (err != EVRInputError.None)
            {
                Error("Failed to get action handle for action: " + actionName);
            }

            // And create action data for convenience.
            actionData = new T();
        }

        // Hate this
        public static void UpdateDigitalActionData(ulong actionHandle, ref InputDigitalActionData_t actionData)
        {
            var err = OpenVR.Input.GetDigitalActionData(actionHandle, ref actionData,
                                                        (uint)Marshal.SizeOf(actionData),
                                                        OpenVR.k_ulInvalidInputValueHandle);

            if (err != EVRInputError.None) { Error("Error getting digital action data!"); }
        }

        public static void UpdateAnalogActionData(ulong actionHandle, ref InputAnalogActionData_t actionData)
        {
            var err = OpenVR.Input.GetAnalogActionData(actionHandle, ref actionData,
                                                       (uint)Marshal.SizeOf(actionData),
                                                       OpenVR.k_ulInvalidInputValueHandle);

            if (err != EVRInputError.None) { Error("Error getting analog action data!"); }
        }

        public static void UpdatePoseActionData(ulong actionHandle, ref InputPoseActionData_t actionData)
        {
            var err = OpenVR.Input.GetPoseActionDataRelativeToNow(actionHandle, ETrackingUniverseOrigin.TrackingUniverseStanding,
                                                                  0f, ref actionData,
                                                                  (uint)Marshal.SizeOf(actionData),
                                                                  OpenVR.k_ulInvalidInputValueHandle);

            if (err != EVRInputError.None) { Error("Error getting pose action data!"); }
        }
        // End hate this

        public static Matrix4x4 GetHMDPose()
        {
            TrackedDevicePose_t hmdPose = new();
            TrackedDevicePose_t hmdGamePose = new();

            var err = OpenVR.Compositor.GetLastPoseForTrackedDeviceIndex(OpenVR.k_unTrackedDeviceIndex_Hmd,
                                                                         ref hmdPose,
                                                                         ref hmdGamePose);
            if (err != EVRCompositorError.None) { Error("Error getting HMD pose!");  }

            return hmdPose.mDeviceToAbsoluteTracking.ToMatrix4x4();
        }
    }
}
