﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FamiliarTurn
{
    public static class DeltaTimeManager
    {
        static Stopwatch Stopwatch = new();

        public static float NewFrame()
        {
            Stopwatch.Stop();
            float deltaTime = (float)Stopwatch.ElapsedMilliseconds / 1000f;
            Stopwatch.Restart();

            return deltaTime;
        }
    }
}
